import React from 'react'
import styles from '../components/MainContent.module.css'
import search from '../assets/search.svg'
import hearteyes from '../assets/hearteyes.svg'
import popper from '../assets/popper.svg'
import bulldart from '../assets/bulldart.svg'
import addexp from '../assets/addexp.svg'
import arrowright from '../assets/arrowright.svg'

import cx from 'classnames'
import { ProgBarVertical } from './ProgBarVertical'
import { ProgBarHoriz } from './ProgBarHoriz'

export const MainContent = () => {
	return (
		<div className={styles.container}>

			<div className={styles.greetSearchWrapper}>
				<div className={styles.greetText}>
         Good Evening, Buddy! 
				</div>

				<div className={styles.searchWrapper}>
					<img className={styles.searchIcon} src={search} alt=""  />
					<input  className={styles.searchBox} type="text" placeholder="Search.."/>
				</div>
			</div>

			<div className={styles.reportCard}>

				<div className={styles.reportContent}>

					<div className={styles.reportImgWrapper}>
						<img className={styles.reportImg} src={hearteyes} alt=""  />
					</div>

					<div>
						<div className={styles.reportTitle}>
							Total no of Experiences
						</div>
						<div className={styles.reportExpCount}>
							#23
						</div>
						<div className={styles.reportExpStart}>
							Since January 23
						</div>
					</div>

				</div>

				<div className={styles.verticalLine}>
				</div>

				<div className={styles.reportContent}>

					<div className={styles.reportImgWrapper}>
						<img className={styles.reportImg} src={bulldart} alt=""  />
					</div>

					<div>
						<div className={styles.reportTitle}>
							Total no of Campaigns
						</div>
						<div className={styles.reportExpCount}>
							#23
						</div>
						<div className={styles.reportExpStart}>
							Since January 23
						</div>
					</div>
				</div>

				<div className={styles.verticalLine}>
				</div>

				<div className={styles.reportContent}>
					<div className={styles.reportImgWrapper}>
						<img  className={styles.reportImg} src={popper} alt=""  />
					</div>
					<div>
						<div className={styles.reportTitle}>
							Total no of Campaigns
						</div>
						<div className={styles.reportExpCount}>
							#23
						</div>
						<div className={styles.reportExpStart}>
							Since January 23
						</div>
					</div>
				</div>

			</div>
			<div className={styles.trendsWrapper}>

				<div className={cx(styles.trendCardHovered,styles.trendCard)}>
					<div className={styles.trendTitle}>Trending</div>
					<div className={styles.trendTitle}>Experience</div>
					<div className={styles.trendImgWrapper}>
						🔥 Engagements - #2345
					</div>
					<div className={styles.trendIdTitle}>Experience ID</div>
					<div className={styles.trendId}>27e18d3hc391hf3c003c3c3</div>
					<div className={styles.trendExpTitle}>Experience Name</div>
					<div className={styles.trendExpName}> Find the actress name</div>

				</div>
				<div className={cx(styles.trendCardHovered,styles.trendCard)}>
					<div className={styles.trendTitle}>Trending</div>
					<div className={styles.trendTitle}>Campaigns</div>
					<div className={styles.trendImgWrapper}>
						🔥 Engagements - #2345
					</div>
					<div className={styles.trendIdTitle}>Campaign ID</div>
					<div className={styles.trendId}>27e18d3hc391hf3c003c3c3</div>
					<div className={styles.trendCampTitle}>Campaign Name</div>
					<div className={styles.trendCampName}>Bollywood Fames</div>
				</div>

				<div className={styles.trendCard}>
					<div className={styles.trendTitle}>Engagement</div>
					<div className={styles.trendTitle}>Trends</div>
					<div className={styles.trendImgWrapper}>
						📅 Last 5 days
					</div>
					<div className={styles.progBarsWrapper}>
						<ProgBarVertical length={40}/>
						<ProgBarVertical length={80}/>
						<ProgBarVertical length={70}/>
						<ProgBarVertical length={100}/>
						<ProgBarVertical length={55}/>
					</div>
				</div>
			</div>

			<div className={styles.expOverviewWrapper}>
				<div className={styles.expOverviewTitle}>
         Experiences Overview 
				</div>

				<div className={styles.addNewExpWrapper}>
					<img className={styles.addExpIcon} src={addexp} alt=""  />
					<div className={styles.addNewExpBtn}>Add new Experience</div>
				</div>
			</div>


			<div className={styles.expOverviewContainer}>
				<div className={styles.expOverviews}>

					<div className={styles.expOverviewTile}>
						<div className={styles.expImgTitleWrapper}>
							<div className={styles.expImgContainer}>
								<img className={styles.expImgPreview} src={popper} alt=""  />
							</div>
							<div className={styles.expNameTypeWrapper}>

								<div className={styles.expNameTitle}>
                    Find the actress name
								</div>
								<div className={styles.expTypeStatusWrapper}>
									<div className={styles.expType}>
										Quiz
									</div>
									<div className={styles.expStatus}>
                    Live
									</div>


								</div>

							</div>
						</div>
						<div className={styles.expBtnWrapper}>
							<img className={styles.expGoToBtn} src={arrowright} alt="" />
						</div>
					</div>

					<div className={styles.expOverviewTile}>
						<div className={styles.expImgTitleWrapper}>
							<div className={styles.expImgContainer}>
								<img className={styles.expImgPreview} src={popper} alt=""  />
							</div>
							<div className={styles.expNameTypeWrapper}>

								<div className={styles.expNameTitle}>
                    Comment your favourite food
								</div>
								<div className={styles.expTypeStatusWrapper}>
									<div className={styles.expType}>
										Quiz
									</div>
									<div className={styles.expStatus}>
                    Draft
									</div>


								</div>

							</div>
						</div>
						<div className={styles.expBtnWrapper}>
							<img className={styles.expGoToBtn} src={arrowright} alt="" />
						</div>
					</div>

					<div className={styles.expOverviewTile}>
						<div className={styles.expImgTitleWrapper}>
							<div className={styles.expImgContainer}>
								<img className={styles.expImgPreview} src={popper} alt=""  />
							</div>
							<div className={styles.expNameTypeWrapper}>

								<div className={styles.expNameTitle}>
                    Find the actress name
								</div>
								<div className={styles.expTypeStatusWrapper}>
									<div className={styles.expType}>
										Quiz
									</div>
									<div className={styles.expStatus}>
                    Live
									</div>


								</div>

							</div>
						</div>
						<div className={styles.expBtnWrapper}>
							<img className={styles.expGoToBtn} src={arrowright} alt="" />
						</div>

					</div>


				</div>
				<div className={styles.trendCard}>
					<div className={styles.trendTitle}>Experiences</div>
					<div className={styles.trendTitle}>Status</div>
					<div className={styles.trendImgWrapper}>
						🌤️ For today
					</div>
					<div className={styles.expStatusWrapper}>
						<div className={styles.expStatusContainer}>
							<div className={styles.expStatusTitle}>
                Live
							</div>
							<ProgBarHoriz length={30}/>
						</div>
						<div className={styles.expStatusContainer}>
							<div className={styles.expStatusTitle}>
                Draft
							</div>
							<ProgBarHoriz length={50}/>
						</div>
						


					</div>
				</div>
				
			</div>
			
			

			
				
		</div>
	)
}
