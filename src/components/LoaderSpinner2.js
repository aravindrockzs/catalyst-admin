import React from 'react'

import './LoaderSpinner2.css'

export const LoaderSpinner2 = () => {
	return (
		<div className="lds-ring"><div></div><div></div><div></div><div></div></div>
	)
}
