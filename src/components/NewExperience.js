import React ,{ useState ,useRef }from 'react'
import styles from './NewExperience.module.css'
import './NewExperience.css'
import search from '../assets/search.svg'
import eye from '../assets/eye.svg'
import rightlongarrow from '../assets/rightlongarrow.svg'
import additem from '../assets/additem.svg'
import { useNavigate,useSearchParams } from 'react-router-dom'

import API from '../axiosconfig'

import { LoaderSpinner2 } from './LoaderSpinner2'


import QuizExperienceCard from './QuizExperienceCard'


import classNames from 'classnames/bind'

import "react-datetime/css/react-datetime.css";


import Step1Experience from './Step1Experience'
import Step2Experience from './Step2Experience'
import Step3Experience from './Step3Experience'


let cx= classNames.bind(styles);


 const NewExperience = () => {

	 const navigate = useNavigate();
	 const[searchParam,setSearchParam]=useSearchParams()

	 const step = Number(searchParam.get("step"))

	 const experience_id = searchParam.get("id")

	 console.log(step);


  	// for emoji
	 const [emoji, setEmoji] = useState('🍊');

	 const [isLoading, setIsLoading] = useState(false);


   const submitHandler=(experienceId)=>{
	

		navigate(`/experiences/add?step=${step<4? step+1:step}&id=${experienceId}`)
	 }


	 const publishExperience=(id)=>{
      setIsLoading(true);
			console.log(id);

			API.post("/brandExperience/publishExperience",{
				"experience_id": id
			}).then((res)=>{
				console.log(res.data)
				setIsLoading(false)

				navigate('/experiences');
			}).catch(e=>{
				setIsLoading(false)
				console.log(e)
			})


	 }

   



	return (
		<div className={styles.container}>

		

		
				<div className={styles.campSearchInputWrapper}>
				<div className={styles.campTitleText}>
         Experiences
				</div>

				<div className={styles.searchWrapper}>
					<img className={styles.searchIcon} src={search} alt=""  />
					<input  className={styles.searchBox} type="text" placeholder="Search.."/>
				</div>
				<div onClick={()=>navigate('/experiences')} className={styles.viewCampWrapper}>
					<img className={styles.viewCampIcon} src={eye} alt=""  />
					<div className={styles.viewCampBtn}>View Experiences</div>
				</div>
			</div>
			<div className={styles.campInstrText}>
				This form allows you to create new experiences.
				<span className={styles.learnMore}> Learn More</span>
			</div>

			<div style={{justifyContent: step===4 ? "center": ""}}className={styles.campFormCardWrapper}>
				{step!==4 &&
					<div className={styles.campFormContainer}>

					{/* ******* Step 1 ******* */}

					{ step ===1 &&  <Step1Experience emoji={emoji} setEmoji={setEmoji} 
					isLoading={isLoading} setIsLoading={setIsLoading} submitHandler={submitHandler}/>}

					{/* ******* Step 2 ****** */}

					{step===2 &&  <Step2Experience 	isLoading={isLoading} 
					setIsLoading={setIsLoading} submitHandler={submitHandler}/>}
				
				

					{/* ******* Step 3 ****** */}

					{step === 3  && <Step3Experience isLoading={isLoading} 
					setIsLoading={setIsLoading} submitHandler={submitHandler}/>}

				

				</div>

			}

					
				

				<div className={styles.campCardSubmitHolder}>
					<QuizExperienceCard emoji={emoji} />
					<div className={styles.progressbarContainer}>
						<div onClick={()=>navigate('/experiences/add?step=1')} className={cx({progressbar1: step>=1},'progressbar')}> 
						</div>
						<div onClick={()=>navigate('/experiences/add?step=2')}  className={cx({progressbar2: step>=2},'progressbar')}> 
						</div>
						<div onClick={()=>navigate('/experiences/add?step=3')}  className={cx({progressbar3: step>=3},'progressbar')}>
						</div>
						<div onClick={()=>navigate('/experiences/add?step=4')}  className={cx({progressbar4: step>=4},'progressbar')}> 
						</div>
					</div>
					<div  onClick={step===4 ? ()=>publishExperience(experience_id): ""}className={styles.campConfirmSubmitBtn}>
						<div className={styles.campConfirmSubmitText}>
							{isLoading ? <LoaderSpinner2/> : step!==4 ? `Step ${step} out of 4`: "Publish Experience"}
						</div>
						{!isLoading && <div className={styles.campConfirmIconHolder}>
							<img src={rightlongarrow} alt="" />
						</div>}
					</div>
				</div>

			</div>



		</div>
	)
}


export default NewExperience;