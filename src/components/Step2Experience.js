import React,{useState} from 'react'

import styles from './NewExperience.module.css';
import additem from '../assets/additem.svg'
import classNames from 'classnames/bind';

import Accordion from './Accordion'
import RewardFill from './RewardFill'
import { useEffect } from 'react';

import {  useSearchParams} from 'react-router-dom';



import API from '../axiosconfig'

const cx= classNames.bind(styles)

const initialState ={
    experience_id:"",
    brand_rewards:[{reward_type:"Badge",reward_id:""}]
}


 const Step2Experience = (props) => {

	const[searchParam,setSearchParam]=useSearchParams()



	 const experience_id = searchParam.get("id")

	 initialState.experience_id = experience_id;




	const {isLoading,setIsLoading,submitHandler} = props


	const [rewards,setRewards] = useState(initialState.brand_rewards);


	const setRewardState = (index,type,data) =>{
		let reward_obj = rewards[index]
		reward_obj[type] = data
    rewards[index] = reward_obj;

		setRewards([...rewards]);
	}

	useEffect(()=>{

		const filteredRewards = rewards.map((reward)=> reward.reward_id)

		const sanitisedState = {
			experience_id:experience_id,
			brand_rewards: filteredRewards
		}
		
    if(isLoading){
			API.post("/brandRewards/assignBrandReward",sanitisedState).then((res)=>{

				console.log(res);
				setIsLoading(false)

				submitHandler(res.data._id)

			}
		).catch(e=>console.log(e))


		}
		
	 },[isLoading])




	return (
		<div>
						<div className={cx('questionsIcon')}>
							<div className={cx('questionsTitleText')}>
								Rewards Setup
							</div>

							<div onClick={()=>{setRewards([...rewards ,{reward_type:"Badge",reward_id:""}])}} 
									className={cx('questionAddIconContainer')}>
								<img src={additem} className={cx('addItemIcon')} alt="" />

							</div>


						</div>

						{
							rewards.map((reward,i)=>{

								return(
									<Accordion startOpen={i===0? true : false} title={`Reward ${i+1} `}>
										<RewardFill onChange={setRewardState} index={i}/>
									</Accordion>

								)
							})
						}

					



					</div>
	)
}


export default Step2Experience;