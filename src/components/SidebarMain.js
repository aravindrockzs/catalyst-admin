import React from 'react'
import styles from './SidebarMain.module.css'
import home from '../assets/home.svg'
import cloud from '../assets/cloud.svg'
import globe from '../assets/globe.svg'
import joystick from '../assets/joystick.svg'
import piechart from '../assets/piechart.svg'
import reward from '../assets/reward.svg'
import chatcall from '../assets/chatcall.svg'


import cx from 'classnames'


import { NavLink, useLocation } from 'react-router-dom'



export const SidebarMain = () => {

	const location = useLocation();

  
	if(location.pathname === '/login' || location.pathname ==='/authorization'){
		return null;
	}
  
	
	return (
		<div className={styles.container}>
			<div  className={styles.navContainer}>

			 	<NavLink  className={({ isActive }) => cx(styles.link,{[styles.active]: isActive})} to="dashboard">

					<div className={styles.navLink}>
						<div className={styles.navLogo}>
							<img src={home}alt=""  />
						</div>
						<div className={styles.navTitle}>
						  Dashboard
						</div>
					</div>
				

				</NavLink>
				
		
				
				<div className={styles.navLink}>
					<div className={styles.navLogo}>
						<img src={cloud} alt=""  />
					</div>
					<div className={styles.navTitle} >
						Brand Space
					</div>

				</div>
				
				<NavLink  className={({ isActive }) => cx(styles.link,{[styles.active]: isActive})} to="campaigns">

					<div className={styles.navLink}>
						<div className={styles.navLogo}>
							<img src={globe} alt=""  />
						</div>
						<div className={styles.navTitle}>
						 Campaigns
						</div>

					</div>

				</NavLink>

				<NavLink  className={({ isActive }) => cx(styles.link,{[styles.active]: isActive})} to="experiences">

					<div className={styles.navLink}>
						<div className={styles.navLogo}>
							<img src={joystick} alt=""  />
						</div>
						<div className={styles.navTitle}>
							Experiences
						</div>

					</div>
				</NavLink>

				<NavLink  className={({ isActive }) => cx(styles.link,{[styles.active]: isActive})} to="rewards">
					<div className={styles.navLink}>
						<div className={styles.navLogo}>
							<img src={reward} alt=""  />
						</div>
						<div className={styles.navTitle}>
							Rewards
						</div>

					</div>
				</NavLink>	
				<div className={styles.navLink}>
					<div className={styles.navLogo}>
						<img src={piechart} alt=""  />
					</div>
					<div className={styles.navTitle}>
						Analytics
					</div>

				</div>


			</div>
			<div className={styles.doodleWrapper}>
				<img className={styles.doodleImg} src={chatcall} alt=""  />
			</div>
		</div>
	)
}
