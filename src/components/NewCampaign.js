import React ,{ useState,useEffect }from 'react'
import styles from './NewCampaign.module.css'
import search from '../assets/search.svg'
import eye from '../assets/eye.svg'
import rightlongarrow from '../assets/rightlongarrow.svg'
import { useNavigate } from 'react-router-dom'
import CampaignCard from './CampaignCard'
import classNames from 'classnames/bind'

import { EmojiSelector } from 'react-emoji-selectors';
import API from '../axiosconfig'
import ToggleSwitch from './ToggleSwitch'

import SearchModal from './SearchModal'
import Portal from './Portal'

import getBrandId from '../helpers/getBrandID'

import {  toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {LoaderSpinner2} from './LoaderSpinner2'




const cx = classNames.bind(styles);




 const NewCampaign = () => {

	 const navigate = useNavigate();

	 const token = getBrandId();

	 

	 const [isLoading,setIsLoading] = useState(false);
	 const [isPortalOpen,setPortal] = useState(false)

	 const [searchModalValue,setSearchModalValue] = useState("");


	 const [ rewardsArr, setRewardsArr] = useState(null);

	 useEffect(()=>{
 
 
		 API.get("/brandRewards/getAllBrandReward").then(function (response) {
			 setRewardsArr(response.data)
		 })
		 .catch(function (error) {
			 console.log(error);
		 })

		
 
	 },[])

	 const initialState= {
		campaign_name: "",
		campaign_description: "",
		campaign_image: "🦁",
		campaign_goal_enabled: false,
		campaign_goal_description:"",
		campaign_goal_winning_criteria: "", 
		campaign_goal_reward: "",
		brand_associated:token,
	}

	 const [campaign,setCampaign] = useState(initialState)

	 const handleInputChange = (e) => {
    const { name, value } = e.target;

    setCampaign((prevCampaign)=>{

			return{
				...prevCampaign,
      [name]: value
			}
      
    });
  };

	

	

  	// for emoji
	 const [emoji, setEmoji] = useState('🦁');
   const [showEmojiPicker, setShowEmojiPicker] = useState(false);

	 //for streak icon campaign card
	 const [streakEnabled,setStreakEnabled] = useState(false);

	 useEffect(()=>{
		 setCampaign((prevCampaign)=>{

			return{
				...prevCampaign,
				campaign_image: emoji,
				campaign_goal_enabled: streakEnabled

			}
			
		 })

	},[emoji,streakEnabled])


	useEffect(()=>{

		setCampaign((prevCampaign)=>{

			return{
				...prevCampaign,
				campaign_goal_reward: searchModalValue._id

			}
		
		})



	},[searchModalValue])



	const submitHandler =()=>{

		setIsLoading(true);

	
		API.post("/brandCampaign/addBrandCampaign", campaign)
		.then(function (response) {
  
			setIsLoading(false);

			// dismiss();
			toast.success('🤩 Campaign Created', {
				position: "top-center",
				autoClose: 5000,
				hideProgressBar: false,
				closeOnClick: true,
				theme:"colored",
				pauseOnHover: false,
				draggable: true,
				progress: undefined,
				});

				console.log(response);


				navigate('/campaigns');


		})
		.catch(function (error) {
			console.log(error);

			setIsLoading(false);

			// dismiss();

			toast.error('😵 Something Happened!', {
				position: "top-center",
				autoClose: 5000,
				hideProgressBar: false,
				theme:"colored",
				closeOnClick: true,
				pauseOnHover: false,
				draggable: true,
				progress: undefined,
				});

			
		});
		
	}
 




	return (
		<div className={styles.container}>


			<Portal open={isPortalOpen} >
				<SearchModal  close={()=>{setPortal(false)}} rewardsArr={rewardsArr}  defaultReward="rewards"
				 setSearchModalValue={setSearchModalValue}/>
			</Portal>

		
				<div className={styles.campSearchInputWrapper}>
				<div className={styles.campTitleText}>
         Campaigns
				</div>

				<div className={styles.searchWrapper}>
					<img className={styles.searchIcon} src={search} alt=""  />
					<input  className={styles.searchBox} type="text" placeholder="Search.."/>
				</div>
				<div onClick={()=>navigate('/campaigns')} className={styles.viewCampWrapper}>
					<img className={styles.viewCampIcon} src={eye} alt=""  />
					<div className={styles.viewCampBtn}>View Campaigns</div>
				</div>
			</div>
			<div className={styles.campInstrText}>
				This form allows you to create new campaign.
				<span className={styles.learnMore}> Learn More</span>
			</div>

			<div className={styles.campFormCardWrapper}>
				<div className={styles.campFormContainer}>
					<div className={styles.campNameHolder}>
						<div className={styles.campNameTitle}>
							Campaign Name
						</div>
						<input className={styles.inputBox} type="text" name="campaign_name" value={campaign.campaign_name}
						placeholder={ "Enter the campaign Name"} onChange={handleInputChange} />

					</div>
					<div className={styles.campDescHolder}>
						<div className={styles.campDescTitle}>
							Campaign Description
						</div>
						<input className={styles.inputBox} type="text" name='campaign_description' value={campaign.campaign_description}
						 placeholder={ "Enter the campaign description"} onChange={handleInputChange} />

					</div>

					<div className={styles.campEmojiStreakWrapper}>
						<div className={styles.campTitleEmojiHolder}>
							<div className={styles.campTitleHeader}>
								Campaign Emoji
							</div>

							<div className={styles.emojiStyle} onClick={() => setShowEmojiPicker(true)}>
								 {emoji}
							</div>

							

								{showEmojiPicker &&
									<EmojiSelector onChange={()=>console.log('working')}
											onClose={() => setShowEmojiPicker(false)} 
											output={setEmoji} 
									/>
								}

							


						</div>
						<div className={styles.campStreakHolder}>

							<div className={styles.campTitleHeader}>
								Campaign Streak
							</div>
							<div className={styles.toggleSwitchHolder}>
								<ToggleSwitch value={streakEnabled} setSwitch={()=>setStreakEnabled(!streakEnabled)}/>
							</div>
							

						</div>


					
					</div>

					{streakEnabled && <div>
							<div className={styles.campDescHolder}>
							<div className={styles.campDescTitle}>
								Campaign Streak Description
							</div>
							<input className={styles.inputBox} type="text" placeholder={ "Campaign Streak Description"}
							name="campaign_goal_description" value={campaign.campaign_goal_description} onChange={handleInputChange} />

						</div>

						<div className={styles.campDescHolder}>
							<div className={styles.campDescTitle}>
								Campaign Streak Winning Criteria
							</div>
							<input className={styles.inputBox} type="text" placeholder={ "Campaign Streak Winning Criteria"} 
							name="campaign_goal_winning_criteria" value={campaign.campaign_goal_winning_criteria}  onChange={handleInputChange} />

						</div>

						<div className={styles.campDescHolder}>
							<div className={styles.campDescTitle}>
								Campaign Streak Reward
							</div>
							<input  onClick={()=>setPortal(true)} value={searchModalValue.brand_reward_name || ""}
							className={styles.inputBox} type="text" placeholder={ "Campaign Streak Reward"} />

						</div>
					</div>
				}
					


				</div>
				

				<div className={styles.campCardSubmitHolder}>
					<CampaignCard emoji={emoji} showStreak={streakEnabled}/>
					
					<div onClick={submitHandler} className={cx('campConfirmSubmitBtn' , {pointerNone : isLoading})}>
						<div className={styles.campConfirmSubmitText}>
							{isLoading? <LoaderSpinner2/> : "Confirm and Submit"}
						</div>
						{!isLoading && <div className={styles.campConfirmIconHolder}>
							<img src={rightlongarrow} alt=""  />
						</div>}
					</div>
				</div>

			</div>


		</div>
	)
}


export default NewCampaign;