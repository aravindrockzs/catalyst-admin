import React,{useState,useEffect} from 'react'
import styles from './NewVoucherElement.module.css'

import classNames from 'classnames/bind'
import rightlongarrow from '../assets/rightlongarrow.svg'
import LoaderSpinner from './LoaderSpinner'

import getBrandId from '../helpers/getBrandID'
import { useNavigate } from 'react-router-dom'

import API from '../axiosconfig'

const cx = classNames.bind(styles);



const initialState= {
	voucher_name: "",
	voucher_description: "",
	voucher_image: "🦁",
	voucher_short_code:"",
  voucher_type: "",
	brand_associated: getBrandId()
}

export const NewVoucherElement = ({emoji}) => {

	const [totalCode, setTotalCode] = useState(5);

	const [isLoading,setIsLoading] = useState(false);

	const [voucherElement, setVoucherElement] = useState(initialState);

	const navigate = useNavigate();

	const handleInputChange =(e)=>{

		const {name,value} = e.target;

		setVoucherElement((prevVoucher)=>{

			return{
				...prevVoucher,
				[name]	: value,
			}
		})
	}

	useEffect(()=>{

		setVoucherElement((prevVoucher)=>{

			return{
				...prevVoucher,
				voucher_image: emoji

			}

		})
	},[emoji])

	const submitHandler = ()=>{

		setIsLoading(true);

		API.post('/brandRewards/addVoucher',voucherElement).then((res)=>{

			setIsLoading(false)
			console.log(res);

		  navigate('/rewards');

			
		}).catch(e=>{

			setIsLoading(false)
      console.log(e);
		})




	}


	return (
		<div>

			<div className={styles.campNameHolder}>
				<div className={styles.campNameTitle}>
					Reward Element Title
				</div>
				<input className={styles.inputBox} type="text" placeholder={ "Enter reward element title"} 
				name={"voucher_name"} value={voucherElement.voucher_name} onChange={handleInputChange}/>

			</div>

			<div className={styles.campNameHolder}>
				<div className={styles.campNameTitle}>
					Reward Element Description
				</div>
				<input className={styles.inputBox} type="text" placeholder={ "Enter reward element description"} 
				 name={"voucher_description"}  value={voucherElement.voucher_description} onChange={handleInputChange}/>

			</div>

			<div className={cx('voucherCodeGenerate')}>

				<div className={cx('voucherCodeHolder')}>
					<div className={styles.campNameTitle}>
						Voucher Short Code
					</div>
					<input className={styles.inputBox} type="text" placeholder={ "Enter the short code"} 
					name={"voucher_short_code"}  value={voucherElement.voucher_short_code} onChange={handleInputChange}/>

				</div>

				<div className={styles.maxAttemptContainer}>

						<div className={styles.campDescTitle}>
								Total Codes
						</div>
						

						<div className={styles.maxAttemptNumber}>

							<input value={totalCode} onChange={(e)=>setTotalCode(e.target.value)}
								className={styles.maxAttemptInput} 
								type="number" />

						</div>
						
				</div> 




			</div>

			<div className={cx('assetsContainer')}>

				<div className={cx('inputFileHolder')}>
					<input className={cx('inputTypeFile')}type="file" name="" id="" />
				</div>

				<div className={cx('orContainer')}>
					<div className={cx('hr')}>
					</div>
					<div className={cx('orTitleText')}>
						or
					</div>

					<div className={cx('hr')}>
					</div>
				</div>

				<div className={cx('pexelFileUpload')}>

					Select asset from pexel

				</div>

			</div>
			<div onClick={submitHandler} className={styles.campConfirmSubmitBtn}>
						<div className={styles.campConfirmSubmitText}>
						{isLoading ? <LoaderSpinner/> : "Confirm and Submit"}
						</div>
						{!isLoading && <div className={styles.campConfirmIconHolder}>
							<img src={rightlongarrow} alt="" />
						</div>}
			</div>

	</div>							
	)
}
