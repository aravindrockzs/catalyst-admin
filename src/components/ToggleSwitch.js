import React from 'react';

import { useId } from 'react'
import './ToogleSwitch.css'

const ToggleSwitch = (props) => {

	const id = useId()

	const {value,setSwitch} = props
	return (
		<div className="toggleContainer">
			<input id={id} checked={value} onChange={setSwitch}
			  type="checkbox"  className="switch"/>
			<label  htmlFor={id}></label>
		</div>
		 
	)
}


export default ToggleSwitch;