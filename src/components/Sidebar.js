import React from 'react'

import  styles from './Sidebar.module.css'
import catalystLogo from '../assets/catalyst-logo.svg'
import addBrand from '../assets/add-brand.png'

export const Sidebar = () => {
	return (
		<div className={styles.container}>
     
			<div className={styles.logosWrapper}>
				<div className={styles.logoContainer}>
					<img className={styles.catalystLogo} src={catalystLogo} alt="" />
				</div>

				<div className={styles.brandsWrapper}>
					<div className={styles.addBrandContainer}>
						<img className={styles.addBrand} src={addBrand} alt="" />
					</div>
					<div className={styles.brandContainer}>
					
					</div>
				</div>

				
			</div>
		  
			 
		</div>

	)
}
