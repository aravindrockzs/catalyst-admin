import React from 'react'
import styles from './QuizExperienceCard.module.css'
import fire from '../assets/fire.svg'

import rightarrow from '../assets/rightarrow.svg'
import rightlongarrow from '../assets/rightlongarrow.svg'
import hourglass from '../assets/hourglass.svg'
import ecg from '../assets/ecg.svg'
import reward from '../assets/reward.svg'
import star from '../assets/star1.svg'

 const QuizExperienceCard = ({emoji,showStreak}) => {

	return (
		<div className={styles.container}>
			<div className={styles.expNameEmojiHolder}>
				<div className={styles.campNameHolder}>
				 	<div className={styles.expNameSm}>Quiz Experience</div>
					<div className={styles.expNameTitle}>Quiz about the diet forms</div>
				</div>
		
				<div className={styles.quizEmojiHolder}>
					<img className={styles.quizStarIcon} src={star} alt="" />
				    {emoji ? <div className={styles.campEmojiIcon}>{emoji}</div>: <img className={styles.campEmojiIcon} src={fire} alt="" /> 	} 
				</div>
			
			</div>
			<div className={styles.campDescInstrWrapper}>
				<div className={styles.campDesc}>
					<div className={styles.expDescTitle}> About Experience</div>
					<div className={styles.campRightArrowHolder}>
						<img className={styles.campRightArrowIcon} src={rightarrow} alt="" />
					</div>
				</div>
				<div className={styles.campInstr}>
					<div className={styles.campInstrTitle}> Rules </div>
					<div className={styles.campRightArrowHolder}>
						<img  className={styles.campRightArrowIcon} src={rightarrow} alt="" />
					</div>
				</div>
			</div>


			<div className={styles.expDescWrapper}>
				<div  className={styles.hourglassIconHolder}>
					<img className={styles.hourglassIcon} src={hourglass} alt="" />
				</div>
				<div className={styles.expDesc}>
					<div className={styles.expDescTitle}> About Experience</div>
				</div>
			</div>

			<div className={styles.expDescWrapper}>
				<div  className={styles.hourglassIconHolder}>
					<img className={styles.ecgIcon} src={ecg} alt="" />
				</div>
				<div className={styles.expDesc}>
					<div className={styles.expDescTitle}> 2 / 3 attempts left</div>
				</div>
			</div>

			<div className={styles.expDescWrapper}>
				<div  className={styles.hourglassIconHolder}>
					<img className={styles.rewardIcon} src={reward} alt="" />
				</div>
				<div className={styles.expDesc}>
					<div className={styles.expDescTitle}> 50% OFF sale voucher </div>
				</div>
			</div>

			<div className={styles.playNowBtnHolder}>
				

				<div className={styles.playNowTitleIconHolder}>
					<div className={styles.campExploreTitle}>Play Now</div>

					<div className={styles.playNow}>
						<img className={styles.playNowIcon} src={rightlongarrow} alt="" />
					</div>
				</div>
			</div>

		</div>
	)
}


export default QuizExperienceCard