import React from 'react'
import {useState,useEffect} from 'react';
import styles from './Experience.module.css'
import search from '../assets/search.svg'
import addexp from '../assets/addexp.svg'
import pencilwrite from '../assets/pencilwrite.svg'
import trash from '../assets/trash.svg'
import sort from '../assets/sort.svg'
import downarrow from '../assets/downarrow.svg'

import classNames from 'classnames/bind';
import Modal from './Modal'
import { useNavigate, useSearchParams} from 'react-router-dom';
import QuizExperienceCard from './QuizExperienceCard';

import API from '../axiosconfig'

const cx = classNames.bind(styles);

const  Experience = () => {

	const navigate= useNavigate();

	const [experiences,setExperience] = useState([])

	useEffect(()=>{
    
		API.get("/brandExperience/getAllBrandExperience").then(res=>{
			setExperience(res.data);
		}).catch(e=>{
			console.log(e)
		})


	},[])
	

	const [isModalOpen, setIsModalOpen] = useState(false);
	return (
		<div className={styles.container}>
			<Modal open={isModalOpen} close={()=>setIsModalOpen(false)}>
				 <QuizExperienceCard />
			</Modal>
			<div className={styles.expSearchInputWrapper}>
				<div className={styles.expTitleText}>
         Experiences
				</div>

				<div className={styles.searchWrapper}>
					<img className={styles.searchIcon} src={search} alt=""  />
					<input  className={styles.searchBox} type="text" placeholder="Search.."/>
				</div>
				<div onClick={()=>navigate('/experiences/add?step=1')}className={styles.addNewExpWrapper}>
					<img className={styles.addExpIcon} src={addexp} alt=""  />
					<div className={styles.addNewExpBtn}>Add new Experience</div>
				</div>
			</div>
			<div className={styles.activeSortContainer}>
				<div className={styles.sortTitleIconWrapper}>
					<div className={styles.sortTitle}>
						Sort
					</div>
					<div className={styles.sortBtn}>
						<img src={sort} alt="" />
					</div>

				</div>
				<div className={styles.activeTitleIconWrapper}>
					<div className={styles.activeTitle}>
						Active
					</div>
					<div className={styles.activeBtn}>
						<img src={downarrow} alt="" />
					</div>

				</div>


			</div>

			<div onClick={()=>setIsModalOpen(true)} className={styles.expTable}>

				<table className={cx('table')}>

					<thead>
						<tr className={cx('rewardHeader')}>
							<th className={cx('rewardSlNo')}>#</th>
							<th className={cx('rewardName')}>Experience Name</th>
							<th className={cx('rewardType')}>Type</th>
							<th className={cx('rewardMax')}>Status</th>
							<th className={cx('rewardUnits')}>Featured </th>
							<th className={cx('rewardActions' ,'tableAlignRight')}>Actions</th>
						</tr>

					</thead>

					<tbody>
					{experiences.map((val,i, key) => {
						console.log(val.featured)
         		 return (
								<tr className={cx('tableRow')} key={key}>
									<td className={cx('rewardSlNo','tableData')}>{i+1}</td>
									<td className={cx('rewardName','tableData')}>
										{val.experience_name}
									</td>
									<td className={cx('tableData')}>{val.experience_type}</td>
									<td className={cx('tableData')}>

										<div className={cx(val.publish_status=== "LIVE" ? "expFeatBtnEnabled" : "expFeatBtnDisabled")}>
											{val.publish_status}
										</div>			

									</td>
									<td className={cx('tableData')}>
										<div className={cx(val.featured=== true ? "expFeatBtnEnabled" : "expFeatBtnDisabled")}>
											{val.featured===true ? "YES" : "NO" }
											
										</div>

									</td>
								<td className={cx('tableData')}>
										<div className={styles.rewardActionWrapper}>
											<div className={styles.campActionWrite}>
												<img className={styles.campWriteIcon}src={pencilwrite} alt="" />
											</div>

											<div className={styles.campActionDelete}>
												<img className={styles.campDeleteIcon}src={trash} alt=""  />
											</div>
										</div>


									</td>
								</tr>
         		  )
							})}
					</tbody>
				</table>

				
			</div>
			
    


		</div>
	)
}


export default Experience;