import React,{useState,useEffect} from 'react'

import styles from './NewExperience.module.css'
import { EmojiSelector } from 'react-emoji-selectors';
import ToggleSwitch from './ToggleSwitch';
import Select from './Select';
import additem from '../assets/additem.svg'
import Datetime from 'react-datetime';
import SearchModal from './SearchModal';

import getBrandId from '../helpers/getBrandID'


import API from '../axiosconfig'
import Portal from './Portal';

const initialState={

    experience_name: "",
    experience_short_description: "",
    experience_long_description: "",
    experience_image: "",
    experience_rules: [""],
    experience_terms: [""],
    experience_type: "",
    attempt_type: "LIMITED",
    max_attempts: 30,
    brand_associated:getBrandId(),
    brand_campaign_associated:"",
    featured: true,
    starts_at:new Date(),
    expires_at:new Date()
}

const Step1Experience = (props) => {

	const {emoji,setEmoji,isLoading,setIsLoading,submitHandler} = props

	const [experience,setExperience] = useState(initialState);
	const [expType,setExpType] = useState("QUIZ");
	const [attemptType,setAttemptType] = useState("LIMITED")

	const [featured,setFeatured] = useState(true);




	const [showEmojiPicker, setShowEmojiPicker] = useState(false);
	const [isPortalOpen,setPortal] = useState(false)

	const [campaigns,setCampaigns] = useState([]);

	const [searchModalValue,setSearchModalValue] = useState("");


	

	

	//for date picker
	const [startDate,setStartDate] = useState(new Date());

	const [endDate,setEndDate] = useState(new Date());


	useEffect(()=>{

		setExperience(prevExperience=>{
			return{
				...prevExperience,
				starts_at: startDate
			}
		})


	},[startDate])


	useEffect(()=>{

		setExperience(prevExperience=>{
			return{
				...prevExperience,
				expires_at: endDate
			}
		})


	},[endDate])



	// campaigns API

	useEffect(()=>{

		API.get("/brandCampaign/getAllBrandCampaigns").then(res=>{
			setCampaigns(res.data)
		})
		.catch(e=>console.log(e))


	},[])

	

	
   //emoji
	useEffect(()=>{
    setExperience(prevExperience=>{
			return{
				...prevExperience,
				experience_image: emoji
			}
		})
	},[emoji])

	//featured

	useEffect(()=>{

		setExperience(prevExperience=>{
			return{
				...prevExperience,
				featured: featured,
			}
		})

	},[featured])

	//exp type

	useEffect(()=>{

		setExperience(prevExperience=>{
			return{
				...prevExperience,
				experience_type: expType,
			}
		})

	},[expType])

	//search Modal Value

	useEffect(()=>{

		setExperience((prevExperience)=>{

			return{
				...prevExperience,
				brand_campaign_associated: searchModalValue._id

			}
		
		})

	},[searchModalValue])

	// upload Experience

   useEffect(()=>{
    if(isLoading){
			API.post("/brandExperience/addBrandExperience",experience).then((res)=>{

				console.log(res);
				setIsLoading(false)

				submitHandler(res.data._id)

			}
		).catch(e=>console.log(e))


		}
		
	 },[isLoading])
  
	

	const renderDay=(props, currentDate, selectedDate)=> {
    // Adds 0 to the days in the days view
    return (
      <td className="rtdActive" {...props}>
        {currentDate.date()}
      </td>
    );
  }

	let inputProps = {
    className: styles.dateTimePickerInput
	}

	
	const handleInputChange =(e)=>{

		const {name,value} = e.target;

		setExperience((prevExperience)=>{

			return{
				...prevExperience,
				[name]: value
			}
		})

	}




	const addInputArr = (name)=>{
		setExperience((prevExperience)=>{

			return{
				...prevExperience,
				[name]: [...prevExperience[name],""]
			}
		})
	}

	const handleInputArraysChange = (e,i,name)=>{

		const {value} = e.target

	
		setExperience((prevExperience)=>{

			const arr = prevExperience[name];
			arr[i] = value;

		  prevExperience[name] = arr;


			return{
				...prevExperience,
	
			}
		})

	}




	return (
		<div className={styles.step1}>

						<Portal open={isPortalOpen}>
							<SearchModal rewardsArr={campaigns} type="campaigns" defaultReward="campaigns"
						 close={()=>{setPortal(false)}} setSearchModalValue={setSearchModalValue}/>

						</Portal>
								
						<div className={styles.campNameHolder}>
							<div className={styles.campNameTitle}>
								Experience Name
							</div>
							<input className={styles.inputBox} type="text" placeholder={ "Enter the experience Name"}
							 name="experience_name" value={experience.experience_name}  onChange={handleInputChange}/>

						</div>
						<div className={styles.campDescHolder}>
							<div className={styles.campDescTitle}>
								Experience Description
							</div>
							<input className={styles.inputBox} type="text" placeholder={ "Enter the experience description"} 
							name="experience_short_description" value={experience.experience_short_description}  onChange={handleInputChange}/>

						</div>

						<div className={styles.campEmojiStreakWrapper}>
							<div className={styles.campTitleEmojiHolder}>
								<div className={styles.campTitleHeader}>
									Experience Emoji
								</div>

								<div className={styles.emojiStyle} onClick={() => setShowEmojiPicker(true)}>
									{emoji}
								</div>

								<div className={styles.emojiSelectorWrapper}>

									{showEmojiPicker &&
										<EmojiSelector 
												onClose={() => setShowEmojiPicker(false)} 
												output={setEmoji} 
										/>
									}


								</div>

							</div>
							<div className={styles.campStreakHolder}>

								<div className={styles.campTitleHeader}>
									Featured Flag
								</div>
								<div className={styles.toggleSwitchHolder}>
									<ToggleSwitch value={experience.featured} setSwitch={()=>setFeatured(!featured)}/>
								</div>
								

							</div>
						</div>

						<div className={styles.campDescHolder}>
							<div className={styles.campDescTitle}>
								Experience Type
							</div>
							
							<Select defaultValue={expType}  setSelect={setExpType} options={["QUIZ","UGC","OPINION"]}/>
							

						</div>

						<div className={styles.campDescHolder}>
							<div className={styles.expRulesAddIconHolder}>
								<div className={styles.expRulesTitle}>
									Experience Rules
								</div>
								<div onClick={()=>addInputArr("experience_rules")} className={styles.addItemHolder}>
									<img className={styles.addItemIcon} src={additem} alt="" />
								</div>

							</div>

							<div className={styles.expRulesInputs}>
								{
									experience.experience_rules.map((val,i)=>{
										return <input type='text' className={styles.inputBox} 
										onChange={(e)=>handleInputArraysChange(e,i,"experience_rules")} value={val}
										placeholder={`Enter rule no ${i+1}`}/>
									})
								}
							</div>
						</div>

						<div className={styles.campDescHolder}>
							<div className={styles.expRulesAddIconHolder}>
								<div className={styles.expRulesTitle}>
									Experience Terms
								</div>
								<div onClick={()=>addInputArr("experience_terms")} className={styles.addItemHolder}>
									<img className={styles.addItemIcon} src={additem} alt="" />
								</div>


							</div>

							<div className={styles.expRulesInputs}>

								{
									experience.experience_terms.map((val,i)=>{
										return <input type="text" className={styles.inputBox} 
										onChange={(e)=>handleInputArraysChange(e,i,"experience_terms")} value={val}
										placeholder={`Enter term no ${i+1}`}/>
									})

								}
							</div>
							

						</div>

						<div className={styles.attemptsMaxContainer}>
							<div className={styles.attemptsContainer}>
								<div className={styles.campDescTitle}>
									Attempts Type
								</div>
								<Select defaultValue={attemptType} setSelect={setAttemptType} options={["LIMITED","UNLIMITED"]}/>
									
								
							</div>

							<div className={styles.maxAttemptContainer}>

								<div className={styles.campDescTitle}>
										Max Attempts
								</div>
								

								<div className={styles.maxAttemptNumber}>

									<input className={styles.maxAttemptInput} type="number" 
									 name='max_attempts' value={experience.max_attempts} onChange={handleInputChange}/>

								</div>
								
							</div> 
							
						</div>



						<div className={styles.campDescHolder}>
							<div className={styles.campDescTitle}>
								Campaigns Associated
							</div>
							<input type="text" onClick={()=>setPortal(true)} 
							className={styles.inputBox} placeholder={"Enter the campaign name"} 
							value={searchModalValue.campaign_name}/>
						</div>

						<div className={styles.campDescHolder}>
							<div className={styles.campDescTitle}>
								Experience starts at
							</div>
							

							<div className={styles.dateTimePickerContainer}>

								<label className={styles.dateTimePickerLabel}>
									<Datetime  renderDay={renderDay} onChange={(e)=>setStartDate(e._d)} 
									value={startDate}
									inputProps={inputProps} initialValue={startDate} closeOnSelect/>
								</label>

							</div>
						</div>


						<div className={styles.campDescHolder}>
							<div className={styles.campDescTitle}>
								Experience ends at
							</div>
							

							<div className={styles.dateTimePickerContainer}>

								<label className={styles.dateTimePickerLabel}>
									<Datetime renderDay={renderDay} className={styles.bgDate} onChange={(e)=>setEndDate(e._d)} 
									value={endDate}
									inputProps={inputProps} initialValue={endDate} closeOnSelect/>
								</label>

							</div>
						</div>

					</div>    
	)
}


export default Step1Experience