import React,{useState} from 'react'

const ClickToggle = (OriginalComponent) => {


	function NewComponent(props){
		const [ isOpen, setIsOpen] = useState(false);


		return <OriginalComponent {...props} changeOpen={ ()=> setIsOpen(!isOpen)}  isOpen={isOpen}/>
	}

	return NewComponent;
	
}

export default ClickToggle;
