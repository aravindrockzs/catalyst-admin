import React from 'react'
import styles from './ProgBarVertical.module.css';

export const ProgBarVertical = (props) => {


	
	return (
		<div className={styles.container}>
			<div className={styles.progressValue} style={{width: '100%', height: `${props.length}%`}} >

			</div>
		</div>
	)
}
