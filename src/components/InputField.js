
import React from "react";
import styles from './InputField.module.css'

const InputField = ({ value, name, placeholder, onChange }) => (
  <div className={styles.container}>
    <input
      value={value}
      name={name}
      className={styles.inputBox}
      placeholder={placeholder}
      onChange={onChange}
    />
  </div>
);

export default InputField;