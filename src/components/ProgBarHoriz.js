import React from 'react'
import styles from './ProgBarHoriz.module.css';

export const ProgBarHoriz = (props) => {

	return (
		<div className={styles.container}>
			<div className={styles.progressValue} style={{height: '100%', width: `${props.length}%`}} >

			</div>
		</div>
	)
}
