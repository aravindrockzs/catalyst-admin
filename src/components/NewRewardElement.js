import React, { useState } from 'react'
import search from '../assets/search.svg'
import eye from '../assets/eye.svg'
import rightlongarrow from '../assets/rightlongarrow.svg'
import classNames from 'classnames/bind'
import { useNavigate } from 'react-router-dom'
import { EmojiSelector } from 'react-emoji-selectors'

import styles from './NewRewardElement.module.css'
import Select from './Select'
import NewBadgeElement from './NewBadgeElement'
import { NewVoucherElement } from './NewVoucherElement'

const cx = classNames.bind(styles)

 const NewRewardElement = () => {

	const [emoji, setEmoji] = useState('🦁');
   const [showEmojiPicker, setShowEmojiPicker] = useState(false);



	const [rewardElType,setRewardElType]= useState('BADGE');

	const navigate= useNavigate();


	return (
		<div className={cx('container')}>
			<div className={cx('campSearchInputWrapper')}>
				<div className={cx('campTitleText')}>
         Rewards
				</div>

				<div className={cx('searchWrapper')}>
					<img className={cx('searchIcon')} src={search} alt=""  />
					<input  className={cx('searchBox')} type="text" placeholder="Search.."/>
				</div>
				<div onClick={()=>navigate('/rewards')} className={cx('viewCampWrapper')}>
					<img className={cx('viewCampIcon')} src={eye} alt=""  />
					<div className={cx('viewCampBtn')}>View reward elements</div>
				</div>
			</div>
			<div className={cx('campInstrText')}>
				This form allows you to create new reward elements for your brand.
				<span className={cx('learnMore')}> Learn More</span>
			</div>

			<div className={cx('rewardContent')}>

				<div className={cx('rewardContentMain')}>
					<div className={cx('rewardElementAssetContainer')}>
						<div className={cx('rewardElementType')}>

							<div className={cx('rewardTypeTitle')}>

								Reward Element Type
							</div>

							<Select defaultValue={rewardElType} options={['BADGE','VOUCHER']} setSelect={setRewardElType}/>
						</div>
						<div className={cx('rewardAsset')}>

							<div className={cx('rewardAssetTitle')}>
								Reward Asset
								
							</div>
							<div>

							<div className={cx('emojiStyle')} onClick={() => setShowEmojiPicker(true)}>
								 {emoji}
							</div>

							

								{showEmojiPicker &&
									<EmojiSelector 
											onClose={() => setShowEmojiPicker(false)} 
											output={setEmoji} 
									/>
								}



							</div>




						</div>


					</div>

					{rewardElType=== "BADGE" && <NewBadgeElement emoji={emoji}/>}
					{rewardElType=== "VOUCHER" &&  <NewVoucherElement emoji={emoji}/>}


				</div>
			




			</div>
		</div>
	)
}


export default NewRewardElement;