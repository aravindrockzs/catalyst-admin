import { useState}  from 'react'
import styles from './Rewardfill.module.css'
import Select from './Select'

import classNames from 'classnames/bind'
import { useEffect } from 'react';
import API from '../axiosconfig';

import Portal from './Portal'
import SearchModal from './SearchModal';

const cx  = classNames.bind(styles);




 const RewardFill = (props) => {
  const {index} =props;
	const [rewardsAssociated,setRewardsAssociated] = useState([]);

	const [defaultRewards,setDefaultRewards] = useState([])
	const [searchModalValue,setSearchModalValue] = useState({});

	const [isPortalOpen,setPortal] = useState(false);

	useEffect(()=>{

		API.get("/brandRewards/getAllBrandReward").then((res)=>{
			setRewardsAssociated(res.data)
			setDefaultRewards(res.data.filter(reward=>reward.brand_reward_type==="BADGE"))
		}).catch(e=>{

			console.log(e)
		})



	},[])

	useEffect(()=>{
		props.onChange(props.index,"reward_id",searchModalValue._id)

	},[searchModalValue])

	const [rewardName, setrewardName] = useState("BADGE")

	const Selectchange = (data) =>{
		setSearchModalValue("");
		setrewardName(data)
		props.onChange(props.index,"reward_type",data)
		setRewardsAssociated(rewardsAssociated.filter(reward=>reward.brand_reward_type===data))
	}


	


	return (
		<div className={cx('container')}>

			<Portal open={isPortalOpen} >
				<SearchModal  close={()=>{setPortal(false)}} rewardsArr={rewardName==="BADGE"? defaultRewards: rewardsAssociated}
				 defaultReward='rewards' setSearchModalValue={setSearchModalValue}/>
			</Portal>

		
			<div className={styles.campDescHolder}>
				<div className={styles.campDescTitle}>
					Reward Type
				</div>
				
				<Select defaultValue={rewardName} setSelect={(data)=>Selectchange(data)}  options={["BADGE","VOUCHER","POINT"]}/>
							

			</div>
			<div>
				<div className={cx('questionSetupTitle')}>
				  Reward to be Associated
				</div>
				<input onClick={()=>setPortal(true)} className={cx('questionBox')}
				 value={searchModalValue.brand_reward_name || "" } type="text" placeholder='Enter the reward name'  />

			</div>
		
			
		</div>
	)
}

export default RewardFill;
