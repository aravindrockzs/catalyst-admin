import React ,{ useEffect, useRef ,useState, createContext} from 'react'

import arrowpink from '../assets/arrowpink.svg'
import classNames from 'classnames/bind';
import styles from './Accordion.module.css';
import ClickToggle from './ClickToggle';


const cx = classNames.bind(styles)



const Accordion = (props) => {

	
	
	const {title,children,isOpen,changeOpen,startOpen,asset,bgColor} = props

	const [contentStyle,setContentStyle] = useState(['accordionContent']);
	

	useEffect(()=>{

		if(startOpen){

			toggleHandler();
		}

     

	},[])



	

	

	
	
	
	const content = useRef(null)
	

	const [maxHeight,setMaxHeight] = useState("0px");

	// useEffect(()=>{

	// 	setMaxHeight(`${content.current.scrollHeight}px` )

	// },[asset])



	const toggleHandler=()=>{
	
		changeOpen();
		setMaxHeight(!isOpen?`${content.current.scrollHeight+1800}px` : "0px");
		

	}





	return (
		<div style={{backgroundColor:bgColor}}className={cx('accordionContainer')}>
			<div onClick ={toggleHandler}  className={cx('accordionTitleIcon')}>
				<div  className={cx('accordionTitle')}>
         {title}
				</div>
				<div className={cx('accordionIconWrapper')}>
					<img className={cx('accordionIcon', { iconRotate: isOpen}) }  src={arrowpink} alt="" />
				</div>

			</div>
			<div style={{maxHeight: maxHeight}} ref={content} 
					className={cx(...contentStyle )}>
						
								
					{children}



			</div>


		</div>
	)
}



export default ClickToggle(Accordion);

// className={cx('accordionContent',{visible: isOpen}