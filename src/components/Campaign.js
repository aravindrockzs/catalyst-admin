import React,{useEffect} from 'react'
import {useState} from 'react';
import styles from './Campaign.module.css'
import search from '../assets/search.svg'
import addexp from '../assets/addexp.svg'
import pencilwrite from '../assets/pencilwrite.svg'
import trash from '../assets/trash.svg'
import sort from '../assets/sort.svg'
import downarrow from '../assets/downarrow.svg'
import Modal from './Modal'
import { useNavigate } from 'react-router-dom';

import API from '../axiosconfig';
import CampaignCard from './CampaignCard';

import classNames from 'classnames/bind';
import PageLoading from './PageLoading';


const cx = classNames.bind(styles);





const Campaign = () => {

	useEffect(()=>{

		setIsLoading((true))

		API.get("brandCampaign/getAllBrandCampaigns").then(res => {
			setIsLoading(false)
			setCampaigns(res.data);
		})
		.catch(e=>{console.log(e)})




	},[])

	const navigate= useNavigate();

	const [isLoading,setIsLoading] = useState(false);

	const [isModalOpen, setIsModalOpen] = useState(false);

	const [campaigns,setCampaigns] = useState([]);
	
	return (
		<div className={styles.container}>
			{isLoading && <PageLoading/>}
			<Modal open={isModalOpen} close={()=>setIsModalOpen(false)}>
				<CampaignCard/>
			</Modal>

			<div className={styles.campSearchInputWrapper}>
				<div className={styles.campTitleText}>
         Campaigns
				</div>

				<div className={styles.searchWrapper}>
					<img className={styles.searchIcon} src={search} alt=""  />
					<input  className={styles.searchBox} type="text" placeholder="Search.."/>
				</div>
				<div onClick={()=>navigate('/campaigns/add')}className={styles.addNewCampWrapper}>
					<img className={styles.addCampIcon} src={addexp} alt=""  />
					<div className={styles.addNewCampBtn}>Add new Campaign</div>
				</div>
			</div>
			<div className={styles.activeSortContainer}>
				<div className={styles.sortTitleIconWrapper}>
					<div className={styles.sortTitle}>
						Sort
					</div>
					<div className={styles.sortBtn}>
						<img src={sort} alt="" />
					</div>

				</div>
				<div className={styles.activeTitleIconWrapper}>
					<div className={styles.activeTitle}>
						Active
					</div>
					<div className={styles.activeBtn}>
						<img src={downarrow} alt="" />
					</div>

				</div>


			</div>

			<div className={styles.campTable}>


				<table className={cx('table')}>

						<thead>

							<tr  className={cx('rewardHeader')}>
								<th className={cx('rewardSlNo')}>#</th>
								<th className={cx('rewardElName')}>Campaign Name</th>
								<th className={cx('rewardType')}> Campaign Description</th>
								<th className={cx('rewardElMax')}>Streak</th>
								<th className={cx('rewardActions','tableAlignRight')}>Actions</th>
							</tr>

						</thead>

						<tbody>

							{campaigns.map((val, i) => {
         		 return (
								<tr onClick={()=>setIsModalOpen(true)} className={cx('tableRow')} key={val._id}>
									<td className={cx('rewardSlNo','tableData')}>{i+1}</td>
									<td className={cx('rewardElName','tableData')}>
										{val.campaign_name}
									</td>
									<td className={cx('rewardElType','tableData')}> {val.campaign_description}</td>
									<td>
										{ val.campaign_goal_enabled ? 
											<div className={styles.campStreakBtnEnabled}>
												Enabled
											</div>:

											<div className={styles.campStreakBtnDisabled}>
												Disabled
											</div>
										}
										  
									</td>
									<td className={cx('tableData')}>
										<div className={styles.rewardActionWrapper}>
											<div className={styles.campActionWrite}>
												<img className={styles.campWriteIcon}src={pencilwrite} alt="" />
											</div>

											<div className={styles.campActionDelete}>
												<img className={styles.campDeleteIcon}src={trash} alt=""  />
											</div>
										</div>

									</td>
								</tr>
         		  )
							})}
						</tbody>






						</table>




			</div>
			
    


		</div>
	)
}


export default Campaign;

// <div className={styles.campStreakBtnDisabled}>
// 						disabled
// 					</div>
