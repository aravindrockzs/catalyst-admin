import React,{useEffect, useState} from 'react'


import LoaderSpinner from './LoaderSpinner';
import styles from './NewBadgeElement.module.css'
import rightlongarrow from '../assets/rightlongarrow.svg';
import { useNavigate } from 'react-router-dom';

import getBrandId from '../helpers/getBrandID';



import API from '../axiosconfig';


const initialState= {
	badge_name: "",
	badge_description: "",
	badge_image: "🦁",
	brand_associated: getBrandId()
}



 const NewBadgeElement = ({emoji}) => {

	const [isLoading,setIsLoading] = useState(false);

	const [badgeElement, setBadgeElement] = useState(initialState);

	const navigate = useNavigate();




	useEffect(()=>{

		setBadgeElement((prevBadge)=>{

			return{
				...prevBadge,
				badge_image: emoji

			}

		})
	},[emoji])


	

	const handleInputChange =(e)=>{

		const {name,value} = e.target;

		setBadgeElement((prevBadge)=>{

			return{
				...prevBadge,
				[name] : value

			}

		})

	}


	const submitHandler = ()=>{

		setIsLoading(true);

		API.post('/brandRewards/addBadge',badgeElement).then((res)=>{

			setIsLoading(false)

		  navigate('/rewards');

			
		}).catch(e=>{

			setIsLoading(false)
      console.log(e);
		})




	}



  
	
	return (

		<div>
			<div className={styles.campNameHolder}>
				<div className={styles.campNameTitle}>
					Reward Element Title
				</div>
				<input className={styles.inputBox} type="text" placeholder={ "Enter reward element title"}
				 name={"badge_name"} value={badgeElement.badge_name} onChange={handleInputChange}
				/>

			</div>

			<div className={styles.campNameHolder}>
				<div className={styles.campNameTitle}>
					Reward Element Description
				</div>
				<input className={styles.inputBox} type="text" placeholder={ "Enter reward element description"}
				name={"badge_description"} value={badgeElement.badge_description} onChange={handleInputChange} />

			</div>

			<div onClick={submitHandler} className={styles.campConfirmSubmitBtn}>
						<div className={styles.campConfirmSubmitText}>
						{isLoading ? <LoaderSpinner/> : "Confirm and Submit"}
						</div>
						{!isLoading && <div className={styles.campConfirmIconHolder}>
							<img src={rightlongarrow} alt="" />
						</div>}
			</div>

		</div>
	
	)
}

export default NewBadgeElement;