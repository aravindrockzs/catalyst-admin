import React from 'react'
import styles from './CampaignCard.module.css'
import fire from '../assets/fire.svg'
import fire1 from '../assets/fire1.svg'
import medal from '../assets/medal.svg'
import rightarrow from '../assets/rightarrow.svg'
import rightlongarrow from '../assets/rightlongarrow.svg'

 const CampaignCard = ({emoji,showStreak}) => {
	return (
		<div className={styles.container}>
			<div className={styles.campNameEmojiHolder}>
				<div className={styles.campNameHolder}>
				 	<div className={styles.campNameSm}>Campaign</div>
					<div className={styles.campNameTitle}>Campaign 1</div>
				</div>
				<div className={styles.campEmojiHolder}>
					{ emoji || <img className={styles.campEmojiIcon} src={fire} alt="" />}
				</div>
			</div>
			<div className={styles.campDescInstrWrapper}>
				<div className={styles.campDesc}>
					<div className={styles.campDescTitle}> Description </div>
					<div className={styles.campRightArrowHolder}>
						<img className={styles.campRightArrowIcon} src={rightarrow} alt="" />
					</div>
				</div>
				<div className={styles.campInstr}>
					<div className={styles.campInstrTitle}> Instruction </div>
					<div className={styles.campRightArrowHolder}>
						<img  className={styles.campRightArrowIcon} src={rightarrow} alt="" />
					</div>
				</div>
			</div>

			<div className={styles.campStreakMedalExploreHolder}>
				<div className={styles.campStreakMedalHolder}>
				{showStreak && <div className={styles.campStreak}>
						<img className={styles.campStreakIcon} src={fire1} alt="" />
					</div>}
					<div className={styles.campMedal}>
						<img className={styles.campMedalIcon} src={medal} alt=""  />
					</div>

				</div>

				<div className={styles.campExploreHolder}>
					<div className={styles.campExploreTitle}>Explore</div>

					<div className={styles.campExploreArrow}>
						<img src={rightlongarrow} alt="" />
					</div>
				</div>

			</div>

		</div>
	)
}


export default CampaignCard