import React from 'react'

import asidebanner from '../assets/asidebanner.svg'

import styles from './Aside.module.css'

export const Aside = () => {
	return (
		<div className={styles.container}>
			<img className={styles.asideBanner} src={asidebanner} alt=""  />
			
		</div>
	)
}
