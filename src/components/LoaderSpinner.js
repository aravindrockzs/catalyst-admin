import React from 'react'
import styles from './LoaderSpinner.module.css'

const LoaderSpinner = ({width,height}) => {
	return (
		<div style={{width:`${width}`,height:`${height}`}} className={styles.loader}>

		</div>
	)
}


export default LoaderSpinner;