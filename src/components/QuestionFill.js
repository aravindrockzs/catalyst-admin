import React ,{useState, useContext} from 'react'
import classNames from 'classnames/bind'
import styles from './QuestionFill.module.css';
import ToggleSwitch from './ToggleSwitch';
import Select from './Select';
import additem from '../assets/additem.svg'
import OptionFill from './OptionFill';
import Accordion from './Accordion';



const cx = classNames.bind(styles)

const QuestionFill = (props) => {


	const {questions,setQuestions,questionIdx} = props

	// console.log(questionIdx,questions,setQuestions);

	const [ select ,setSelect] = useState("Image")
	const [toggle,setToggle] = useState(false)


	const newOptionObj ={
		option_text: "",
		option_asset: "",
		option_asset_type: "IMAGE",
		correct_option:false
}

	

	const setOptionState=(index,type,data)=>{
    
		let optionObj = questions[questionIdx].options[index];
		optionObj[type] = data;


		setQuestions([...questions])

	}

	const questionChange =(e)=>{
		const {name,value} = e.target
		props.onChange(props.questionIdx,name,value)
	}

	const addOption =()=>{

		const optionArr = questions[questionIdx].options;

		const newOptionArr = [...optionArr,newOptionObj];

		questions[questionIdx].options = newOptionArr;

		setQuestions([...questions])


	}

	const selectQuestionAsset=(data)=>{
			props.onChange(props.questionIdx,"question_asset_type",data)
			setSelect(data);
	}


	return ( 
		<div className={cx('container')}>
			<input className={cx('questionBox') }type="text" placeholder='Enter question' name="question" 
			onChange={(e)=>questionChange(e)}/>
			<div className={cx('questionSetupTitle')}>
				Question Asset Setup
			</div>
			<div className={cx('assets')}>
				<div className={cx('toggleSwitch')}>
					<div className={cx('questionSwitchContainer')}>
						<ToggleSwitch value={toggle} setSwitch={()=>setToggle(!toggle)}/>
					</div>

				{toggle &&	<div className={cx('selectContainer')}>
						<Select	defaultValue={select} setSelect={(data)=>selectQuestionAsset(data)} options={["Image","UGC","OPINION"]}/>

					</div>}
				

				</div>
			
				{	toggle && <div className={cx('assetsContainer')}>

					<div className={cx('inputFileHolder')}>
						<input className={cx('inputTypeFile')}type="file" name="" id="" />
					</div>

					<div className={cx('orContainer')}>
						<div className={cx('hr')}>
						</div>
						<div className={cx('orTitleText')}>
							or
						</div>

						<div className={cx('hr')}>
						</div>
					</div>

					<div className={cx('pexelFileUpload')}>

						Select asset from pexel

					</div>
					
				</div> }


				<div className={cx('questionsIcon')}>
							<div className={cx('questionsTitleText')}>
								Options Setup
							</div>
							<div onClick={addOption}
							     className={cx('questionAddIconContainer')}>
								<img src={additem} className={cx('addItemIcon')} alt="" />

							</div>


						</div>
				
			</div>

			{
				questions[questionIdx].options.map((option,i,arr)=>{
				
					return (

						<Accordion startOpen={true} title={`Option ${i+1}`} asset={option.asset} bgColor={"#FFFFFF"}>
							<OptionFill option={questions[questionIdx].options[i]}  optionIdx={i} onChange={setOptionState}
							toggle={option.asset}/> 
						</Accordion>
		
					)
				})
			}

			
			



		</div>
	)
}


export default QuestionFill;