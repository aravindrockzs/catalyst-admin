import React,{useState ,useEffect} from 'react';
import closeicon from '../assets/closeicon.svg'
import search from '../assets/search.svg'

import styles from './SearchModal.module.css'
import classNames from 'classnames/bind';


const cx = classNames.bind(styles);




 const SearchModal = (props) => {

	const {close,rewardsArr,setSearchModalValue,defaultReward,type} = props

	const [rewards,setRewards] = useState(rewardsArr || []);

  useEffect(() => {
		setRewards(rewardsArr);
	}, [close])

	const clickHandler=(reward)=>{
		setSearchModalValue(reward)
		close()

	}
	
	

	return (
			<div className={styles.modalContentWrapper}>
				<div onClick={close} className={styles.closeModal}>

					<div className={styles.closeIconWrapper}>
						<img  className={styles.closeIcon} src={closeicon} alt=""  />
					</div>
					<div className={styles.closeText}> Close </div>

				</div>

				<div className={styles.modalContent}>
					<div className={styles.searchWrapper}>
						<img className={styles.searchIcon} src={search} alt=""  />
						<input  className={styles.searchBox} type="text" placeholder="Search.."/>
					</div>

					{(defaultReward=== ("BADGE" || "VOUCHER")) && <div className={ cx('tableHeight')}>
						<table className={cx('table')}  >

							<thead>
								<tr className={cx('rewardHead')}>
									<td className={cx('rewardSlNo')}>Sl No</td>
									<td className={cx('rewardName')}>Reward Name</td>
									<td className={cx('rewardType')}>Reward Type</td>
								</tr>

							</thead>

							<tbody>
							{
							  defaultReward ==="BADGE" && rewards.map((reward,i)=>{
									return(
												<tr onClick={()=>clickHandler(reward)} className={cx('rewardBody','tableData')}>
													<td className={cx('rewardSlNo', 'tableLeft')}>
														{i+1}
													</td>
													<td className={cx('rewardName')}>
														{reward.badge_name}
													</td>
													<td className={cx('rewardType', 'tableRight')}>
														{"BADGE"}
													</td>
												</tr>



									)	
								})
							}

							{
							  defaultReward ==="VOUCHER" && rewards.map((reward,i)=>{
									return(
												<tr onClick={()=>clickHandler(reward)} className={cx('rewardBody','tableData')}>
													<td className={cx('rewardSlNo', 'tableLeft')}>
														{i+1}
													</td>
													<td className={cx('rewardName')}>
														{reward.voucher_name}
													</td>
													<td className={cx('rewardType', 'tableRight')}>
														{"VOUCHER"}
													</td>
												</tr>



									)	
								})
							}

							</tbody>

							




						</table>


					</div>}

				{	defaultReward==="campaigns" && <div className={ cx('tableHeight')}>
						<table className={cx('table')}  >

							<thead>
								<tr className={cx('rewardHead')}>
									<td className={cx('rewardSlNo')}>Sl No</td>
									<td className={cx('rewardName')}>Campaign Name</td>
									<td className={cx('rewardType')}></td>
								</tr>

							</thead>

							<tbody>
						

						
							  {rewards.map((reward,i)=>{
									return(
												<tr onClick={()=>clickHandler(reward)} className={cx('rewardBody','tableData')}>
													<td className={cx('rewardSlNo', 'tableLeft')}>
														{i+1}
													</td>
													<td className={cx('rewardName')}>
														{reward.campaign_name}
													</td>
													<td className={cx('rewardType', 'tableRight')}>
													</td>
												</tr>)	
									})
								}
							

							</tbody>

							




						</table>


						</div>

				}

				{	defaultReward==="rewards" && <div className={ cx('tableHeight')}>
						<table className={cx('table')}  >

							<thead>
								<tr className={cx('rewardHead')}>
									<td className={cx('rewardSlNo')}>Sl No</td>
									<td className={cx('rewardName')}>Reward Name</td>
									<td className={cx('rewardType')}>Reward Type</td>
								</tr>

							</thead>

							<tbody>
						

						
							  {rewards.map((reward,i)=>{
									return(
												<tr onClick={()=>clickHandler(reward)} className={cx('rewardBody','tableData')}>
													<td className={cx('rewardSlNo', 'tableLeft')}>
														{i+1}
													</td>
													<td className={cx('rewardName')}>
														{reward.brand_reward_name}
													</td>
													<td className={cx('rewardType', 'tableRight')}>
														{reward.brand_reward_type}
													</td>
												</tr>)	
									})
								}
							

							</tbody>

							




						</table>


						</div>

				}
				
				
					
					
          
				</div>
					

				
			</div>
	
	)
}


export default SearchModal;