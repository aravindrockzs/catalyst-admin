import React,{useState, useEffect} from 'react'

import {useSearchParams} from 'react-router-dom'
import styles from './NewExperience.module.css'
import classNames from 'classnames/bind'

import additem from '../assets/additem.svg'

import Accordion from './Accordion'
import QuestionFill from './QuestionFill'

import API from '../axiosconfig'


const cx = classNames.bind(styles)


const initialState={
	total_questions: 1,
	brand_experience_associated: "",
	question_and_options:[
		{
			question: "",
			question_asset: "",
			question_asset_type: "IMAGE",
			options:[{
					option_text: "",
					option_asset: "",
					option_asset_type: "IMAGE",
					correct_option:false
			}]
		}
	],
	winning_conditions:"",
  time_to_complete: 5

}

const Step3Experience = (props) => {


	const {isLoading,setIsLoading,submitHandler} = props
   
	const[searchParam,setSearchParam]=useSearchParams()
	const experience_id = searchParam.get("id")

	initialState.brand_experience_associated = experience_id;




   const [experience,setExperience] = useState(initialState);

	 const newQuestion = {
		question: "",
		question_asset: "",
		question_asset_type: "IMAGE",
		options:[{
				option_text: "",
				option_asset: "",
				option_asset_type: "IMAGE",
				correct_option:false
		}]
	}
	
	const [questions,setQuestions] =useState(initialState.question_and_options);

	const setQuestionState=(index,type,data)=>{

		let questionObj = questions[index];
		questionObj[type] = data;


		setQuestions([...questions])

	}

	const handleInputChange = (e) => {
    const {name,value} = e.target;


		setExperience((prevExperience) =>{

			return{
				...prevExperience,
				[name]: value
			}
		})



	}

	//uploading experience

	useEffect(() => {

		const sanitisedState = {
			...experience,
			question_and_options: questions
		}

		if(isLoading){
			API.post("/brandExperienceTypes/addQuizExperience",sanitisedState).then((res)=>{

				console.log(res);
				setIsLoading(false)

				submitHandler(res.data._id)

			}
		).catch(e=>console.log(e))
		}




	},[isLoading])

	return (
		<div>
							<div className={styles.attemptsMaxContainer}>
								<div className={styles.totalQtnContainer}>

										<div className={styles.campDescTitle}>
												Total Questions
										</div>
										

										<div className={styles.maxAttemptNumber}>

											<input  onChange={handleInputChange}
												className={styles.totalQtnInput} value={experience.total_questions}
												type="number" name="total_questions" />

										</div>
										
									</div> 
									<div className={styles.totalQtnContainer}>

										<div className={styles.campDescTitle}>
												Time to complete
										</div>
										

										<div className={styles.totalQtnInputWrapper}>

											<input value={experience.time_to_complete} onChange={handleInputChange}
												className={styles.timeToComplete}  name='time_to_complete'
												type="number" />

										</div>
										
									</div> 
									
								</div>

							<div className={styles.campNameHolder}>
								<div className={styles.campNameTitle}>
									Winning Criteria
								</div>
								<input className={styles.inputBox} type="text" placeholder={ "Enter the  winning criteria" }
								 onChange={handleInputChange} value={experience.winning_conditions} name='winning_conditions' />

							</div>


						<div className={cx('questionsContainer')}>

							<div className={cx('questionsIcon')}>
								<div className={cx('questionsTitleText')}>
									Questions Setup
								</div>
								<div onClick={()=>{setQuestions([...questions,newQuestion])}} 
										className={cx('questionAddIconContainer')}>
									<img src={additem} className={cx('addItemIcon')} alt="" />

								</div>


							</div>
					

								
							{
								questions.map((question,i)=>{
									return (	

										<Accordion startOpen={i===0? true : false}  title={`Question ${i+1}`} >
											<QuestionFill questions={questions} questionIdx={i} setQuestions={setQuestions}
											  onChange={setQuestionState}/>
										</Accordion>


									)
								})
							}
						</div>

						

						


					</div>
	)
}


export default Step3Experience;