import React, { useEffect } from 'react'
import {useState} from 'react';
import styles from './Rewards.module.css'
import search from '../assets/search.svg'
import addexp from '../assets/addexp.svg'
import pencilwrite from '../assets/pencilwrite.svg'
import trash from '../assets/trash.svg'
import sort from '../assets/sort.svg'
import downarrow from '../assets/downarrow.svg'
import Modal from './Modal'


import CampaignCard from './CampaignCard';
import classNames from 'classnames/bind';
import { useLocation , useNavigate } from 'react-router-dom';

import API from '../axiosconfig';
import Select from './Select';
import BadgeTable from './BadgeTable';
import VoucherTable from './VoucherTable'

const cx = classNames.bind(styles)

const Rewards = () => {

	const navigate= useNavigate();


	const [badgesElData,setBadgesElData] = useState([]);

	const [voucherElData,setVoucherElData] = useState([]);

	const [defaultReward, setDefaultReward] = useState('BADGE')

	const [reward,setReward]= useState([]); 
	



	useEffect(()=>{

		if(badgesElData.length ===0 && defaultReward==="BADGE"){

		
			API.get("/brandRewards/getAllBadges").then((res)=>{
				setBadgesElData(res.data)
	
			}).catch(e=>console.log(e))
		
		}

		if(voucherElData.length ===0 && defaultReward==="VOUCHER"){
			API.get("/brandRewards/getAllVoucher").then((res)=>{
				setVoucherElData(res.data)
	
			}).catch(e=>console.log(e))
		
		}

	

	},[defaultReward])

	useEffect(()=>{

		API.get("/brandRewards/getAllBrandReward").then((res)=>{

			setReward(res.data);
		}).catch(e=>{
			console.log(e);
		})



	},[])




	const location = useLocation()
	

	const path = location.pathname



	const [isModalOpen, setIsModalOpen] = useState(false);
	return (
		<div className={styles.container}>
			<Modal open={isModalOpen} close={()=>setIsModalOpen(false)}>
				<CampaignCard/>
			</Modal>
			<div className={styles.campSearchInputWrapper}>
				<div className={styles.campTitleText}>
         Rewards
				</div>

				<div className={styles.searchWrapper}>
					<img className={styles.searchIcon} src={search} alt=""  />
					<input  className={styles.searchBox} type="text" placeholder="Search.."/>
				</div>
				<div onClick={path==='/rewards' ? ()=>navigate('/rewards/elements/add'):()=>navigate('/rewards/rewards/add') }
				   className={styles.addNewCampWrapper}>
					<img className={styles.addCampIcon} src={addexp} alt=""  />
					
						
					{path==='/rewards' ? <div className={styles.addNewCampBtn}> Add new Reward Element</div>
					
					: <div className={styles.addNewCampBtn}> Add new Reward </div>}
						
						
					
				</div>
			</div>


			<div className={cx('rewardsElementsContainer')}>
				<div onClick={()=>navigate('/rewards')} 
					className={cx('rewardsElements',{active: path==='/rewards'})}>
          Reward Elements
				</div>
				<div  onClick={()=>navigate('/rewards/rewards')}className={cx('rewards',{active: path==='/rewards/rewards'})}>
					Rewards
				</div>

			</div>

			<div className={styles.activeSortContainer}>
				<div className={styles.sortTitleIconWrapper}>
					<div className={styles.sortTitle}>
						Sort
					</div>
					<div className={styles.sortBtn}>
						<img src={sort} alt="" />
					</div>

				</div>
				<div >

					<Select defaultValue={defaultReward} setSelect={setDefaultReward}  border={'black'} options={['BADGE', 'VOUCHER','POINTS']}/>
			

				</div>


			</div>

			{path==='/rewards' && 
			
				<div className={styles.campTable}>
					<table className={cx('table')}>

						<thead>

							<tr  className={cx('rewardHeader','tableHead')}>
								<th className={cx('rewardSlNo')}>#</th>
								<th className={cx('rewardElName')}>Reward Name</th>
								<th className={cx('rewardType')}>Type</th>
								<th className={cx('rewardElMax')}>Primary Asset</th>
								<th className={cx('rewardActions','tableAlignRight')}>Actions</th>
							</tr>

						</thead>

						<tbody>

							{defaultReward=== "BADGE" && badgesElData.map((val, i) => {
								return (
									<BadgeTable val={val} i={i} setIsModalOpen={setIsModalOpen}/>
								)
							})}

							{defaultReward=== "VOUCHER" && voucherElData.map((val, i) => {
								return (
									<VoucherTable val={val} i={i} setIsModalOpen={setIsModalOpen}/>
								)
							})}
						</tbody>






						</table>

					
				</div> 
			}

			{path==='/rewards/rewards' && 
			
				<div className={styles.campTable}>

						<table className={cx('table')}>

							<thead>
								<tr className={cx('rewardHeader')}>
									<th className={cx('rewardSlNo')}>#</th>
									<th className={cx('rewardName')}>Reward Name</th>
									<th className={cx('rewardType')}>Type</th>
									<th className={cx('rewardMax')}>Max Rewards</th>
									<th className={cx('rewardUnits')}>Units</th>
									<th className={cx('rewardActions' ,'tableAlignRight')}>Actions</th>
								</tr>

							</thead>

							<tbody>

							{reward.map((val,i, key) => {
         		 return (
								<tr className={cx('tableRow')} key={key}>
									<td className={cx('rewardSlNo','tableData')}>{i+1}</td>
									<td className={cx('rewardName','tableData')}>
										{val.brand_reward_name}
									</td>
									<td className={cx('tableData')}>{val.brand_reward_type}</td>
									<td className={cx('tableData')}>{val.max_rewards}</td>
									<td className={cx('tableData')}>{val.reward_units}</td>
									<td className={cx('tableData')}>
										<div className={styles.rewardActionWrapper}>
											<div className={styles.campActionWrite}>
												<img className={styles.campWriteIcon}src={pencilwrite} alt="" />
											</div>

											<div className={styles.campActionDelete}>
												<img className={styles.campDeleteIcon}src={trash} alt=""  />
											</div>
										</div>

									</td>
								</tr>
         		  )
							})}


							</tbody>
						




						</table>

					

					

				</div> 
			}


			
    


		</div>
	)
}


export default Rewards;