import React,{useState} from 'react'
import classNames from 'classnames/bind'
import styles from './Login.module.css'

import { useNavigate } from 'react-router-dom'

import API from '../axiosconfig'

const cx = classNames.bind(styles);

const initialState={
	email:"",
	password_hash:""
}
const Login = () => {

	const navigate = useNavigate();

	const [loginDetails,setLoginDetails] = useState(initialState)

	

	

	const submitHandler=()=>{

		API.post('/brandManager/loginBrandManager',loginDetails).then((res)=>{

			
			localStorage.setItem('authentication_token',res.data.token)

       navigate('/authorization')

		}).catch(e=>{
			console.log(e)
		})


	   
	}



	const handleInputChange=(e)=>{
    
		const {name,value} = e.target;

		setLoginDetails((prevLoginDetails)=>{

			return{
				...prevLoginDetails,
				[name]: value
			}
		})
  
	}
	return (
		<div className={cx('container')}>

			<p style={{padding: '10px'}}>
				Welcome to login page
			</p>
        <div style={{padding: '10px'}}>
					<label>
						Username
						<input type="text" placeholder="Enter Username" name="email" required
						
						value={loginDetails.email} onChange={handleInputChange} autoComplete="true"/>

					</label>

					
				</div>

				<div style={{padding: '10px'}}>

					<label>
					Password
					<input type="text" placeholder="Enter password" name="password_hash" required
					value={loginDetails.password_hash} onChange={handleInputChange}/>

					</label>



				</div>
			

			


			<button  onClick={submitHandler} type="submit">Login</button>
			
  </div>
	)
}




export default Login;