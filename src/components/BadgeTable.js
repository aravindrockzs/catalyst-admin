import React from 'react'

import styles from './BadgeTable.module.css'
import classNames from 'classnames/bind'
import pencilwrite from '../assets/pencilwrite.svg'
import trash from '../assets/trash.svg'

const cx = classNames.bind(styles);



const BadgeTable = ({val,i,setIsModalOpen}) => {

	return (
		<tr onClick={()=>setIsModalOpen(true)} className={cx('tableRow')} >
			<td  className={cx('rewardSlNo','tableData')}>{i+1}</td>
			<td className={cx('rewardElName','tableData')}>
				{val.badge_name}
			</td>
			<td className={cx('rewardElType','tableData')}>{"BADGE"}</td>
			<td className={cx('tableData')}>
				<div className={styles.primaryAsset}>
					<div className={cx('assetLogo')}>
						{val.badge_image}
					</div>

				</div>
			</td>
			<td className={cx('tableData')}>
				<div className={styles.rewardActionWrapper}>
					<div className={styles.campActionWrite}>
						<img className={styles.campWriteIcon}src={pencilwrite} alt="" />
					</div>

					<div className={styles.campActionDelete}>
						<img className={styles.campDeleteIcon}src={trash} alt=""  />
					</div>
				</div>

			</td>
		</tr>
	)
}

export default BadgeTable