import React,{useState, useRef, useEffect } from 'react'

import downarrow from '../assets/downarrow1.svg'
import ClickToggle from './ClickToggle';
import classNames from 'classnames/bind';
import styles from './Select.module.css';

let cx = classNames.bind(styles)

const Select = ({defaultValue,setSelect,options, isOpen,changeOpen,border}) => {

	 const blackBorder ={
		 border: "1px dashed #000000"
	 }



	const content= useRef(null);


	function selectHandler(e){
		setSelect(e.target.textContent);
	}

	const clickHandler =()=>{
		changeOpen();
		// console.log(content)
	}

	return (
		<div style={border==="black" ? blackBorder :{}} onClick={clickHandler} className={styles.container}>
			<div className={styles.titleContainer}>

				<div className={styles.titleText}>
		   		{defaultValue}

				</div>

				

				<div className={styles.arrow}>

					<img className={styles.arrowIcon} src={downarrow} alt="" />

				</div>


			</div>
			
			<div ref={content} onClick={selectHandler} 
				className={cx('optionsContainer', {optionsContainerActive:isOpen})}>

					{

						options.map(value=>{
							return <div className={styles.optionValue}>{value}</div>
											
						})


					}
			</div>
		</div>
	)
}


export default ClickToggle(Select);

// style={{height:`${isOpen ? content.current.scrollheight+40: 40}px`}}