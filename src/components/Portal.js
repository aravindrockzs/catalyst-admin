import React from 'react'
import ReactDOM from 'react-dom'
import styles from './Portal.module.css'

const Portal = ({open,close,children}) => {

	if(!open) return null;
	return  ReactDOM.createPortal(

		<div className={styles.container}>
			<div className={styles.box1}> </div>

      <div className={styles.box2}>
				{children}
			</div>
			<div className={styles.box3}></div>
			
		</div>
		,
		document.getElementById('portal')
	)
}






export default Portal;