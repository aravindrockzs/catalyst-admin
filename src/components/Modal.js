import React from 'react';
import closeicon from '../assets/closeicon.svg'

import styles from './Modal.module.css'

 const Modal = ({open,children,close}) => {
	 
	

	if(!open) return null;
	return (
		<div className={styles.container}>
			
	
			<div className={styles.modalContentWrapper}>
				<div onClick={close} className={styles.closeModal}>

					<div className={styles.closeIconWrapper}>
						<img  className={styles.closeIcon} src={closeicon} alt=""  />
					</div>
					<div className={styles.closeText}> Close </div>

				</div>

				<div className={styles.modalContent}>

					<div className={styles.campCardPreviewText}>
						<div className={styles.campCardTextSample}>This is how it</div>
						<div className={styles.campCardTextSample}>will look in</div>
						<div className={styles.campCardTextSample}>the consumer</div>
						<div className={styles.campCardTextSample}>interface</div>
					</div>

					
					{children}
					
          
				</div>
					

				
			</div>
		</div>
	)
}


export default Modal;