import React ,{ useState } from 'react'
import styles from './NewReward.module.css'
import search from '../assets/search.svg'
import eye from '../assets/eye.svg'
import Select from './Select'

import rightlongarrow from '../assets/rightlongarrow.svg'

import { useNavigate } from 'react-router-dom'
import SearchModal from './SearchModal'
import Portal from './Portal'

import classNames from 'classnames/bind'

import getBrandId from '../helpers/getBrandID'
import { useEffect } from 'react'
import API from '../axiosconfig'
import {  toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { LoaderSpinner2 } from './LoaderSpinner2'


const cx = classNames.bind(styles)

const initialState={
	brand_associated: getBrandId(),
	brand_reward_name: "",
	brand_reward_type: 'BADGE',
	brand_reward : "",
	max_rewards: 1,
	rewards_issued: 0,
	reward_units: 1,
	
}

const NewReward = () => {
	const [defaultReward, setDefaultReward] = useState('BADGE')
	const [searchModalValue,setSearchModalValue] = useState("");

	const [isPortalOpen,setPortal] = useState(false)


	const [badgesData,setBadgesData] = useState([]);

	const [voucherData,setVoucherData] = useState([]);

	const [pointsData,setPointsData] = useState([]);

	const [isLoading,setIsLoading] = useState(false);



	const [rewardsArr,setRewardsArr] = useState([])

	const [reward,setReward] = useState(initialState);

	const navigate= useNavigate();

	const handleInputChange =(e)=>{
		const {name,value} = e.target

		setReward((prevReward)=>{
			
			return{
				...prevReward,
				[name]: value
				
			}
		})

	}


	useEffect(()=>{
		setSearchModalValue('');
		
		if( defaultReward==="BADGE"){

			setRewardsArr(badgesData)

			setReward((prevReward)=>{
				return{
					...prevReward,
					brand_reward_type: defaultReward,
				}
			})

			if(badgesData.length ===0 ){
	
				API.get("/brandRewards/getAllBadges").then((res)=>{
					setBadgesData(res.data)
					setRewardsArr(res.data)
		
				}).catch(e=>console.log(e))
	
			}
		

		}

		
		if( defaultReward==="VOUCHER"){
			 setRewardsArr(voucherData);

			 setReward((prevReward)=>{
				return{
					...prevReward,
					brand_reward_type: defaultReward,
				}
			})

			if(voucherData.length ===0 ){
				API.get("/brandRewards/getAllVoucher").then((res)=>{
					setVoucherData(res.data)
					setRewardsArr(res.data)
		
				}).catch(e=>console.log(e))
			
			}




		}
		
	},[defaultReward])




	useEffect(()=>{

		setReward((prevReward)=>{

			return{
				...prevReward,
				brand_reward: searchModalValue._id

			}
		
		})

	},[searchModalValue])


	const rewardAssociatedInputValue = ()=>{

		if(typeof searchModalValue === 'object' && searchModalValue !== null)
		{
			if(defaultReward==="BADGE"){
				 return searchModalValue.badge_name;
			}

			if(defaultReward==="VOUCHER"){
				return searchModalValue.voucher_name;
		  }


		}

		else return "";
    

	}

	const submitHandler= ()=>{
		  API.post("/brandRewards/createBrandReward",reward)
			.then((res)=>{
				setIsLoading(false)
				toast.success('🤩 Reward Created', {
				position: "top-center",
				autoClose: 5000,
				hideProgressBar: false,
				closeOnClick: true,
				theme:"colored",
				pauseOnHover: false,
				draggable: true,
				progress: undefined,
				})

				console.log(res)

				navigate('/rewards/rewards')
			}
			)
			.catch(function (error) {
				console.log(error);
	
				setIsLoading(false);
	
				// dismiss();
	
				toast.error('😵 Something Happened!', {
					position: "top-center",
					autoClose: 5000,
					hideProgressBar: false,
					theme:"colored",
					closeOnClick: true,
					pauseOnHover: false,
					draggable: true,
					progress: undefined,
					});
	
				
			})
	}





	return (
		<div className={cx('container')}>

			<Portal open={isPortalOpen} >
				<SearchModal  close={()=>{setPortal(false)}} rewardsArr={rewardsArr} defaultReward={defaultReward}
				 setSearchModalValue={setSearchModalValue}/>
			</Portal>
		
      <div className={cx('campSearchInputWrapper')}>
				<div className={cx('campTitleText')}>
         Rewards
				</div>

				<div className={cx('searchWrapper')}>
					<img className={cx('searchIcon')} src={search} alt=""  />
					<input  className={cx('searchBox')} type="text" placeholder="Search.."/>
				</div>
				<div onClick={()=>navigate('/rewards')} className={cx('viewCampWrapper')}>
					<img className={cx('viewCampIcon')} src={eye} alt=""  />
					<div onClick={()=>navigate('/rewards/rewards')}className={cx('viewCampBtn')}>View rewards</div>
				</div>
			</div>
			<div className={cx('campInstrText')}>
				This form allows you to create new rewards for your brand.
				<span className={cx('learnMore')}> Learn More</span>
			</div>

			<div className={cx('rewardContent')}>
				<div className={cx('rewardContentMain')}>
					<div className={cx('rewardType')}>

						<div className={cx('rewardTypeTitle')}>

							Reward Element Type
						</div>

						<Select defaultValue={defaultReward} options={['BADGE','VOUCHER']} setSelect={setDefaultReward}/>
					</div>

					<div className={styles.campNameHolder}>
						<div className={styles.campNameTitle}>
							Reward Name
						</div>
						<input className={styles.inputBox} type="text" placeholder={ "Enter reward name"} 
						name={"brand_reward_name"}
						value={reward.brand_reward_name} onChange={handleInputChange}/>

					</div>

					<div className={styles.campNameHolder}>
						<div className={styles.campNameTitle}>
							Reward Element Associated
						</div>
						<input className={styles.inputBox} type="text" placeholder={ "Enter reward element associated "}
						onClick={()=>setPortal(true)} value={rewardAssociatedInputValue()}/>

					</div>

					<div className={styles.maxRewardsContainer}>
							<div className={styles.totalQtnContainer}>

									<div className={styles.campDescTitle}>
										 Max rewards
									</div>
									

									<div className={styles.maxAttemptNumber}>

										<input value={reward.max_rewards} className={styles.totalQtnInput} type="number" 
										onChange={handleInputChange} name={"max_rewards"}/>

									</div>
									
							</div> 
							<div className={styles.totalQtnContainer}>

									<div className={styles.campDescTitle}>
											Reward Units
									</div>
									

									<div className={styles.totalQtnInputWrapper}>

										<input value={reward.reward_units} onChange={handleInputChange}
											className={styles.timeToComplete}  
											type="number"  name={"reward_units"}/>

									</div>
									
							</div> 
							
						</div>

						<div onClick={submitHandler} className={cx("campConfirmSubmitBtn",{pointerNone: isLoading})}>
							<div className={styles.campConfirmSubmitText}>
							{false? <LoaderSpinner2/> : "Confirm and Submit"}
							</div>
							{!isLoading && <div className={styles.campConfirmIconHolder}>
								<img src={rightlongarrow} alt="" />
							</div>}
						</div>



				</div>

				


			</div>

		</div>
	)
}


export default NewReward;