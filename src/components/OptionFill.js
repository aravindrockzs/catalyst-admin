import React ,{useState} from 'react'
import classNames from 'classnames/bind'
import styles from './OptionFill.module.css';
import ToggleSwitch from './ToggleSwitch';
import Select from './Select';




const cx = classNames.bind(styles)

const OptionFill = (props) => {



	const [ select ,setSelect] = useState("Image")

	const [toggle,setToggle] = useState(false)

	const handleChecked=(e)=>{
    
		const {name} = e.target

		props.onChange(props.optionIdx,name,!props.option.correct_option)
	}

	const optionChange=(e)=>{
    
		const {name,value} = e.target
     
		props.onChange(props.optionIdx,name,value)

	}

	const selectOptionAsset=(data)=>{
		props.onChange(props.optionIdx,"option_asset_type",data)
		setSelect(data);
}


	return ( 
		<div className={cx('container')}>
			<input className={cx('questionBox') }type="text" placeholder='Enter option' name="option_text"
			onChange={(e)=>optionChange(e)} />

				<div >
					<label className={cx('correctOptionContainer')} >
						
						<input  className={cx('correctInput')} checked={props.option.correct_option} 
							onChange={handleChecked} type="checkbox" name="correct_option"/>
						Correct Option
						<span></span>
					</label>

				</div>
			<div className={cx('questionSetupTitle')}>
				Option Asset Setup
			</div>
			<div className={cx('assets')}>
				<div className={cx('toggleSwitch')}>
					<div className={cx('questionSwitchContainer')}>
						<ToggleSwitch value={toggle} setSwitch={()=>setToggle(!toggle)}/>
					</div>

				

				{toggle &&	<div className={cx('selectContainer')}>
						<Select	defaultValue={select} setSelect={selectOptionAsset} options={["Image","UGC","OPINION"]}/>

					</div>}
				

				</div>
			
				{	toggle && <div className={cx('assetsContainer')}>

					<div className={cx('inputFileHolder')}>
						<input className={cx('inputTypeFile')}type="file" name=""  />
					</div>

					

					<div className={cx('orContainer')}>
						<div className={cx('hr')}>
						</div>
						<div className={cx('orTitleText')}>
							or
						</div>

						<div className={cx('hr')}>
						</div>
					</div>

					<div className={cx('pexelFileUpload')}>

						Select asset from pexel

					</div>
					
				</div> }
				
			</div>
			



		</div>
	)
}


export default OptionFill;