import React,{useEffect,useState} from 'react'

import styles from './AuthorizedBrands.module.css';

import API from '../axiosconfig'
import classNames from 'classnames/bind';

import { useNavigate} from 'react-router-dom'

const cx = classNames.bind(styles);



 const AuthorizedBrands = () => {

	const [brands,setBrands] = useState([]);

	const navigate = useNavigate();


	useEffect(()=>{

		

		API.get("brandManager/getAuthorizedBrands").then(
				(res)=>{
					setBrands(res.data.brand_manager_roles)
				}
			).catch(e=>console.log(e))


	},[])

	const authorizeBrand=(brand)=>{

		if(brand.brand_verified){
			

			API.get(`/brandManager/generateAccessToken?brand_id=${brand._id}`).then((res)=>{


				localStorage.setItem('authorization_token',res.data.token)

				navigate('/dashboard')
			}).catch(e=>console.log(e))
		}

		





	}



	return (
		<div  className={cx('container')}>
      {
				brands.map(brand=>{
					return<div>
								{brand.brand.brand_name}
								<button onClick={()=>authorizeBrand(brand.brand)}>
									{brand.brand.brand_verified ? "authorize" : "pending" }

								</button>
								</div>
					
				})
			}

		</div>
	)
}


export default AuthorizedBrands;