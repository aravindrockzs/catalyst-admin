import axios from 'axios';

import  { API_URL } from '../src/config'

const API = axios.create({
  baseURL: API_URL,
  timeout: 10000,
  headers: {
    'Content-Type': 'application/json',
  },
});

API.interceptors.request.use(
  config => {
    const token = localStorage.getItem('authorization_token');

    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    } else {
        
      const authentication_token = localStorage.getItem('authentication_token');
      config.headers.Authorization = `Bearer ${authentication_token}`;

      if(!authentication_token) delete API.defaults.headers.common.Authorization;
    }
    return config;
  },

  error => Promise.reject(error)
);



export default API;