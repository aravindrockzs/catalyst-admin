
import { Routes, Route, Navigate } from 'react-router-dom';
import { BrowserRouter as Router } from 'react-router-dom';


import { MainContent } from '../src/components/MainContent'

import { SidebarMain } from '.././src/components/SidebarMain';
import { Sidebar } from '.././src/components/Sidebar';
import { Aside } from '.././src/components/Aside';

import {ToastContainer} from 'react-toastify'


import styles from './App.module.css'
import Campaign from './components/Campaign';
import NewCampaign from './components/NewCampaign';
import Experience from './components/Experience';
import NewExperience from './components/NewExperience';
import Rewards from './components/Rewards';
import NewRewardElement from './components/NewRewardElement';
import NewReward from './components/NewReward';
import Login from './components/Login'
import AuthorizedBrands from './components/AuthorizedBrands';



function App() {
  return (

    <div className={styles.container}>
      <Router>
        <ToastContainer/>

        <Sidebar/>
        <SidebarMain/>
        
     
       
        <Routes>

          
          
          <Route path="/" element={<Navigate to="/login"/>}  />

          <Route path="/login" element={<Login />} />
          <Route path="/authorization" element={<AuthorizedBrands />} />
          <Route path="dashboard" element={<MainContent/>}/>
          <Route path="campaigns" element={<Campaign/>}/>
          <Route path="campaigns/add" element={<NewCampaign/>}/>
          <Route path="experiences" element={<Experience/>}/>
          <Route path="experiences/add" element={<NewExperience/>}/>
          <Route path="rewards" element={<Rewards/>}/>
          <Route path="rewards/rewards" element={<Rewards/>}/>
          <Route path="rewards/elements/add"  element={<NewRewardElement/>}/>
          <Route path="rewards/rewards/add" element={<NewReward/>} />
          

        </Routes>
        <Aside/>
      </Router>
    </div>

  );
}

export default App;
